#pragma once

class Point2c {
public:
	char x;
	char y;

	Point2c();
	Point2c(int x, int y) : x(x), y(y) {}

	// copy constructor (c copy)
	Point2c(Point2c *&c) : x(c->x), y(c->y) {}
	Point2c(Point2c &c) : x(c.x), y(c.y) {}

	// define overloaded + (plus) operator
	Point2c operator+ (const Point2c& c) const {
		Point2c result;
		result.x = (this->x + c.x);
		result.y = (this->y + c.y);
		return result;
	}
	void operator+= (const Point2c& c) {
		this->x = (this->x + c.x);
		this->y = (this->y + c.y);
	}
};

class Point2d {
public:
	double x;
	double y;

	Point2d();
	Point2d(double x, double y) : x(x), y(y) {}

	// copy constructor (c copy)
	Point2d(Point2d *&c) : x(c->x), y(c->y) {}
	Point2d(Point2d &c) : x(c.x), y(c.y) {}

	// define overloaded + (plus) operator
	Point2d operator+ (const Point2d& c) const {
		Point2d result;
		result.x = (this->x + c.x);
		result.y = (this->y + c.y);
		return result;
	}
	void operator+= (const Point2d& c) {
		this->x = (this->x + c.x);
		this->y = (this->y + c.y);
	}
};

class Point2i {
public:
	int x;
	int y;

	Point2i();

	Point2i(int x, int y) : x(x), y(y) {}

	// copy constructor (c copy)
	Point2i(Point2i *&c) : x(c->x), y(c->y) {}
	Point2i(Point2i &c) : x(c.x), y(c.y) {}

	// define overloaded + (plus) operator
	Point2i operator+ (const Point2i& c) const {
		Point2i result;
		result.x = (this->x + c.x);
		result.y = (this->y + c.y);
		return result;
	}
	void operator+= (const Point2i& c) {
		this->x = (this->x + c.x);
		this->y = (this->y + c.y);
	}
};

class Point3i {
public:
	int x;
	int y;
	int z;

	Point3i();

	Point3i(int x, int y, int z) : x(x), y(y), z(z) {}

	// copy constructor (c copy)
	Point3i(Point3i *&c) : x(c->x), y(c->y), z(c->z) {}
	Point3i(Point3i &c) : x(c.x), y(c.y), z(c.z) {}

	// define overloaded + (plus) operator
	Point3i operator+ (const Point3i& c) const {
		Point3i result;
		result.x = (this->x + c.x);
		result.y = (this->y + c.y);
		result.z = (this->z + c.z);
		return result;
	}
	void operator+= (const Point3i& c) {
		this->x = (this->x + c.x);
		this->y = (this->y + c.y);
		this->z = (this->z + c.z);
	}
};