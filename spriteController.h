#pragma once

#include "sprite.h"
#include "Simplelist/SimpleList.h"

class spriteController {
protected:
	SimpleList<SpriteClass*> sprites;
public:
	spriteController();
	~spriteController();

	void add(SpriteClass *sprite);	
	void render();
	void update();
	SpriteClass *getSprite(char id);

};

