// 
// 
// 
#include "sprite.h"
#include <math.h>

void SpriteClass::init() {
	setDirection(90);
	setSpeed(0);
	id = 0;	
	MoveHasChanged = true;
	die = false;
	ttl = 0;
}

void SpriteClass::render() {
	u8g->drawXBMP(position.x, position.y, dimensions.x, dimensions.y, bitmap);
}

SpriteClass::SpriteClass(U8GLIB *u8g, char Width, char Height, unsigned char *bitmap) {
	this->u8g = u8g;
	this->dimensions.x = Width;
	this->dimensions.y = Height;
	this->bitmap = bitmap;
	init();
}

void SpriteClass::update() {
	if (ttl > 0 && millis() > ttl) {
		die = true;
	}
	
	// update movement Vector..
	if (MoveHasChanged) {
		moveCache.x = (double)cos(DirectionRadians) * speed;
		moveCache.y = (double)sin(DirectionRadians) * -speed;
		MoveHasChanged = false;
	}
	position.x += moveCache.x;
	position.y += moveCache.y;
}

void SpriteClass::setDirection(int degrees) {
	double rad = 57.2957795f;
	// convert degrees to radian.
	DirectionRadians = degrees / rad;
	MoveHasChanged = true;
}

void SpriteClass::setSpeed(double speed) {
	this->speed = speed;
	MoveHasChanged = true;
}

void SpriteClass::setTTL(unsigned long timeMS) {
	ttl = millis() + timeMS;
}

//SpriteClass Sprite;

