
#define cloud800a_width 20
#define cloud800a_height 11
#define cloud800a_id 2

static unsigned char cloud800a_bits[] U8G_PROGMEM = {
	0x80, 0x07, 0x00, 0xc0, 0x1e, 0x00, 0x20, 0x30, 0x00, 0x20, 0x40, 0x00,
	0x3c, 0xc0, 0x07, 0x46, 0x00, 0x0c, 0x01, 0x00, 0x08, 0x01, 0x40, 0x08,
	0x01, 0xc0, 0x05, 0x3e, 0xe4, 0x07, 0xe0, 0x3f, 0x00 };
