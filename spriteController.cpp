#include "spriteController.h"


spriteController::spriteController() {
	
}

spriteController::~spriteController() {}

void spriteController::add(SpriteClass *sprite) {
	sprites.push_back(sprite);
}

void spriteController::render() {

	for (SimpleList<SpriteClass*>::iterator itr = sprites.begin(); itr != sprites.end(); ++itr) {
		SpriteClass *sprite = *itr;
		sprite->render();
	}
}

void spriteController::update() {
	
	for (SimpleList<SpriteClass*>::iterator itr = sprites.begin(); itr != sprites.end(); ++itr) {
		SpriteClass *sprite = *itr;
		if (sprite->die) {
			sprites.erase(itr);
			delete sprite;
			continue;
		}


		sprite->update();
	}
}

SpriteClass * spriteController::getSprite(char id) {

	for (SimpleList<SpriteClass*>::iterator itr = sprites.begin(); itr != sprites.end(); ++itr) {
		SpriteClass *sprite = *itr;
		if (sprite->id == id) {
			return sprite;
		}
	}
}
