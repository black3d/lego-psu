#include "U8glib.h"
#include "sprite.h"
#include "stdlib.h"		//itoa() function included in stdlib.h
#include <SimpleTimer.h>
#include "spriteController.h"

// XBM files
#include "c64ballon.h"
#include "steamTrain.h"
#include "cloud.h"
#include "smoke.h"

U8GLIB_SH1106_128X64 u8g(10, 9, 12, 11, 13);			// OLED
SpriteClass *ballong;
SpriteClass *tog;
spriteController spriteController1;

// the timer object
SimpleTimer tPsu;
SimpleTimer tSmoke;

char sprite_x_speed = 1;
char sprite_y_speed = 1;
char x = 0;
char y = 0;
char i2abuffer[7];			// The ASCII of the integer will be stored in this char array
long randNumber;

int PSUvolt;				// made global for wider avaliblity throughout a sketch if needed, example a low voltage alarm, etc

void draw(void) {
	// graphic commands to redraw the complete screen should be placed here
	// u8g.drawRFrame(0, 0, u8g.getWidth(), 20, 1);
	u8g.drawStr(13, 0, "LEGO Power Supply");
	u8g.drawHLine(0, 11, u8g.getWidth());

	spriteController1.render();

	u8g.drawHLine(0, u8g.getHeight() - 10, u8g.getWidth());
	u8g.drawStr(35, u8g.getHeight() - 9, i2abuffer);
	u8g.drawStr(1, u8g.getHeight() - 9, "Volt:      Amp: 700mA");

}

void u8g_prepare(void) {
	u8g.setFont(u8g_font_6x10);
	u8g.setFontRefHeightExtendedText();
	u8g.setDefaultForegroundColor();
	u8g.setFontPosTop();
}

int getBandgap(void) { // Returns actual value of Vcc (x 100)

#if defined(__AVR_ATmega1280__) || defined(__AVR_ATmega2560__)
	// For mega boards
	const long InternalReferenceVoltage = 1115L;  // Adjust this value to your boards specific internal BG voltage x1000
	// REFS1 REFS0          --> 0 1, AVcc internal ref. -Selects AVcc reference
	// MUX4 MUX3 MUX2 MUX1 MUX0  --> 11110 1.1V (VBG)         -Selects channel 30, bandgap voltage, to measure
	ADMUX = (0 << REFS1) | (1 << REFS0) | (0 << ADLAR) | (0 << MUX5) | (1 << MUX4) | (1 << MUX3) | (1 << MUX2) | (1 << MUX1) | (0 << MUX0);

#else
	// For 168/328 boards
	const long InternalReferenceVoltage = 1056L;  // Adjust this value to your boards specific internal BG voltage x1000
	// REFS1 REFS0          --> 0 1, AVcc internal ref. -Selects AVcc external reference
	// MUX3 MUX2 MUX1 MUX0  --> 1110 1.1V (VBG)         -Selects channel 14, bandgap voltage, to measure
	ADMUX = (0 << REFS1) | (1 << REFS0) | (0 << ADLAR) | (1 << MUX3) | (1 << MUX2) | (1 << MUX1) | (0 << MUX0);

#endif
	delay(50);  // Let mux settle a little to get a more stable A/D conversion
	// Start a conversion  
	ADCSRA |= _BV(ADSC);
	// Wait for it to complete
	while (((ADCSRA & (1 << ADSC)) != 0));
	// Scale the value
	int results = (((InternalReferenceVoltage * 1023L) / ADC) + 5L) / 10L; // calculates for straight line value 
	return results;
}

void getPsuVolt(void) {
	PSUvolt = getBandgap();  //Determins what actual Vcc is, (X 100), based on known bandgap voltage
	/*Serial.print("Battery Vcc volts =  ");
	Serial.println(PSUvolt);*/
	itoa(PSUvolt, i2abuffer, 10);
}

void addSmoke(void) {
	SpriteClass *train = spriteController1.getSprite(steamTrain_id);

	SpriteClass *sprite = new SpriteClass(&u8g, smoke_width, smoke_height, smoke_bits);
	sprite->setDirection(110);
	sprite->setSpeed(0.3f);
	sprite->position.y = train->position.y;
	sprite->position.x = train->position.x + 30;
	sprite->setTTL(13000);
	spriteController1.add(sprite);
}

void loadSprites(void) {
	SpriteClass *sprite;
	
	ballong = new SpriteClass(&u8g, c64Ballon_width, c64Ballon_height, c64Ballon_bits);
	ballong->id = c64Ballon_id;
	spriteController1.add(ballong);

	tog = new SpriteClass(&u8g, steamTrain_width, steamTrain_height, steamTrain_bits);
	tog->id = steamTrain_id;
	spriteController1.add(tog);
	
	for (int i = 0; i < 5; i++) {
		sprite = new SpriteClass(&u8g, cloud800a_width, cloud800a_height, cloud800a_bits);
		sprite->setDirection(180);
		sprite->setSpeed(0.5f);
		sprite->position.y = 15 + random(15);
		sprite->position.x = random(128 * 2);
		spriteController1.add(sprite);
	}
}

void setup(void) {
	// u8g.setRot180();	// flip screen, if required.
	Serial.begin(115200);
	Serial.println("LEGO Power Supply loaded.");
	Serial.println("\r\n\r\n");

	u8g_prepare();
	loadSprites();

	randomSeed(analogRead(0));

	PSUvolt = 0;
	tPsu.setInterval(1000, getPsuVolt);
	tSmoke.setInterval(4000, addSmoke);	
}

void loop(void) {
	//randNumber = random(128);
	//itoa(randNumber, i2abuffer, 10);
	
	tog->position.x = 35 + x / 2;
	tog->position.y = 33;
	ballong->position.x = x;
	ballong->position.y = y;

	// render the picture loop
	u8g.firstPage();
	do {
		draw();
	} while (u8g.nextPage());

	// measure and print update time.
	unsigned long start = micros();
	spriteController1.update();
	unsigned long stop = micros();
	Serial.print("Update time micro: ");
	Serial.println(stop - start);

	// Update Movement
	x = x + sprite_x_speed;
	y = y + sprite_y_speed;
	if (x >= 128 - c64Ballon_width) sprite_x_speed = -1;
	if (x <= 1) sprite_x_speed = 1;
	if (y >= 45 - c64Ballon_height) sprite_y_speed = -1;
	if (y <= 3) sprite_y_speed = 1;

	// rebuild the picture after some delay
	delay(35);

	// timers.
	tPsu.run();
	tSmoke.run();
}
