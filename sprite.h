// sprite.h.h

#ifndef _SPRITE
#define _SPRITE

#include "U8glib.h"
#include "definitions.h"

#if defined(ARDUINO) && ARDUINO >= 100
	#include "Arduino.h"
#else
	#include "WProgram.h"	// not tested
#endif

class SpriteClass {
	protected:
		U8GLIB *u8g;		// pointer to draw		
		double DirectionRadians;
		unsigned char *bitmap;
		double speed;
		
		// cache to speed up direction calculation by 10x
		bool MoveHasChanged;
		Point2d moveCache;

		unsigned long ttl;	// time to live in ms
	public:
		Point2d position;
		Point2c dimensions;		
		bool die;	// auto die from SpriteController
		char id;

		SpriteClass(U8GLIB *u8g, char Width, char Height, unsigned char *bitmap);
		void init();
		void render();
		void setDirection(int degrees);
		void setSpeed(double speed);
		void setTTL(unsigned long timeMS);
		void update();
};

extern SpriteClass Sprite;

#endif
