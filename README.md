Lego regulated power supply.
This is developed using visual micro and vs2013

### What is this repository for? ###

This will be the code for running a regulated 12v PSU gray area.
This will give analog speed and not just the old 7 step.
This will include a animated oled screen for visualisation.

Version 0.x

### How do I get set up? ###

HW:
cpu: Arduino Nano ATmega328
Screen: OLED SPI 1.3" SainSmart is used. with U8gl
	OLED:    UNO:    Nano:
	3.3v     3.3v    17 3.3v
	GND      GND     04 gnd
	sdin     9       d09
	sclk     10      d10
	dc       11      d11
	rst      13      d13
	cs       12      d12
Power: ?
Potmeter: ?
Amp sensor: ?
Voltage sensor: ?
Regulator: ESC 10A

* Summary of set up
* Configuration
* Dependencies: 
	Time!
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact
